import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Bookings } from './entities/cars.entity';
import { Car } from './entities/car.entity';
import { BookingModule } from './booking/booking.module';
import { StatsModule } from './stats/stats.module';

@Module({
  imports: [TypeOrmModule.forRoot({
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'postgres',
    password: 'postgres',
    database: 'booking',
    entities: [Bookings, Car],
    synchronize: true,
    name: 'mainConnection',
  }), BookingModule, StatsModule],
})
export class AppModule {}