import { Module } from '@nestjs/common';
import { BookingService } from 'src/booking/booking.service';
import { BookingController } from 'src/booking/booking.controller';

@Module({
    controllers: [BookingController],
    providers: [BookingService]
})
export class BookingModule{};