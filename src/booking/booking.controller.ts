import { Controller, Get, Post, Param, Body } from '@nestjs/common';
import { BookingService } from './booking.service';
import { car } from "./booking.service";

@Controller("booking")
export class BookingController {
  constructor(private readonly appService: BookingService) {}

  @Get()
  getCars(): Promise<car[]>  {
    return this.appService.getCars();
  }

  @Post(':id')
  bookingCar(@Param() params, @Body() body): Promise<car[] | string> {
    return this.appService.bookingCar(params.id, body);
  }

}
