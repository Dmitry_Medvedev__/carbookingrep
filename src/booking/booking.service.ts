import { Injectable } from '@nestjs/common';
import { InjectConnection } from '@nestjs/typeorm';
import * as dayjs from 'dayjs'
import { Connection } from 'typeorm';
import calc from '../utils/calc';

export interface car{
  id: number,
  cash: string,
  start: Date,
  finish: Date,
  isActive: boolean,
  month: number,
  message?: string,
}

const DAYS_3 = 259200000;
const MESSAGE_NOT_AVAILABLE = 'В данный момент времени машина недоступна, повторите попытку позже!';
const MESSAGE_START_ERROR = 'Начало аренды не может осуществляться в субботу или воскресение!';
const MESSAGE_END_ERROR = 'Окончание аренды не может осуществляться в субботу или воскресение';
const MESSAGE_ERROR_DAYS = 'Ошибка! Количество дней не в пределах 30';


@Injectable()
export class BookingService {
  constructor(
    @InjectConnection('mainConnection')
    private mainConnection: Connection,
  ) {}
  
  async getCars() : Promise<car[]> {
    return await this.mainConnection.query(`SELECT * FROM bookings`);
  }

  async bookingCar(id, body): Promise<car[] | string> {
    const startInMs = Date.parse(body.startedDate) - DAYS_3;
    const finishInMs = Date.parse(body.finishDate) + DAYS_3;
    let isEmpty = await this.mainConnection.query(`
    SELECT * FROM car WHERE (
      '${(new Date(startInMs)).toISOString()}', '${(new Date(finishInMs)).toISOString()}'
    ) OVERLAPS (
      "startedDate", "finishDate") AND "car"."bookingsId" = '${id}'`);
    if(isEmpty.length !== 0) return MESSAGE_NOT_AVAILABLE;
    const start = dayjs(body.startedDate);
    const finish = dayjs(body.finishDate);
    if((start.toDate().getDay() === 0) || (start.toDate().getDay() === 6)) return MESSAGE_START_ERROR;
    if((finish.toDate().getDay() === 0) || (finish.toDate().getDay() === 6)) return MESSAGE_END_ERROR;
    const days = Math.ceil(finish.diff(start, 'days', true));
    if(1 > days || days > 30) return MESSAGE_ERROR_DAYS;
    const cash = calc(days);
    await this.mainConnection.query(`INSERT INTO car ("startedDate", "finishDate", "bookingsId"
    ) VALUES (
      '${body.startedDate}', '${body.finishDate}', '${id}')`);
    return `Успешно! Стоимость аренды - ${cash} рублей`;
  }
}
