import { Controller, Get, Param } from '@nestjs/common';
import { StatsService } from './stats.service';
import { responseStats } from './stats.service';

@Controller('stats')
export class StatsController {
    constructor(private readonly statsService: StatsService) {}

    @Get()
    getStatsAll() : Promise<string>{
        return this.statsService.getStatsAll();
    }

    @Get(':id')
    getStatsOne(@Param() params) : Promise<responseStats>{
        return this.statsService.getStatsOne(params.id);
    }
}
