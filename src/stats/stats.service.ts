import { Injectable } from '@nestjs/common';
import { InjectConnection } from '@nestjs/typeorm';
import * as dayjs from 'dayjs'
import { Connection } from 'typeorm';
import calcStats from 'src/utils/calcStats';

export interface responseStats {
  number: string,
  workload: string,
}

@Injectable()
export class StatsService {
  constructor(
    @InjectConnection('mainConnection')
    private mainConnection: Connection,
  ) {}
  
  async getStatsOne(id) : Promise<responseStats> {
    const car = await this.mainConnection.query(`SELECT * FROM bookings WHERE id = ${id}`);
    const month = dayjs().get('month');
    const year = dayjs().get('year');
    const maxDays = dayjs().daysInMonth();
    const bookings = await this.mainConnection.query(`
      SELECT * FROM car WHERE (
        '${year}-${month}-01T00:00:00.000Z', '${year}-${month+1}-${maxDays}T23:59:59.999Z'
      ) OVERLAPS (
        "startedDate", "finishDate"
      ) AND "car"."bookingsId" = '${id}'
    `);
    const sum = calcStats(bookings, month);
    const workload = sum / dayjs().daysInMonth() * 100;
    return {number: car[0].federalNum, workload: `${workload.toFixed(1)}%`,}
  }

  async getStatsAll() : Promise<string> {
    const cars = await this.mainConnection.query(`SELECT * FROM bookings`);
    const month = dayjs().get('month');
    const year = dayjs().get('year');
    const maxDays = dayjs().daysInMonth();
    const stats = await this.mainConnection.query(`SELECT * FROM car WHERE (
      '${year}-${month}-01T00:00:00.000Z', '${year}-${month+1}-${maxDays}T23:59:59.999Z'
    ) OVERLAPS (
      "startedDate", "finishDate")`);
    const sum = calcStats(stats, month);
    const workload = sum / (dayjs().daysInMonth() * cars.length) * 100;
    return `Общая загруженность: ${workload.toFixed(1)}%`
  }
}
