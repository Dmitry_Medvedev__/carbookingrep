import * as dayjs from 'dayjs'

export default function calcStats(arrayOfItems, month) {
    let sum = 0;
    arrayOfItems?.forEach(element => {
        const start = dayjs(element.startedDate);
        const finish = dayjs(element.finishDate);
        if((finish.get('month') === month) && (start.get('month') !== month)) {
        sum += finish.get('date');
        }
        else if((start.get('month') === month) && (finish.get('month') !== month)){
        sum += start.daysInMonth() - start.get('date');
        }
        else if((finish.get('month') === month) && (start.get('month') === month)){
        sum += finish.get('date') - start.get('date');
        }
        else if((finish.get('month') !== month) && (start.get('month') !== month))
        {
        sum += dayjs().daysInMonth();
        }
    });
    return sum;
}