const RATE = 1000;
const FIVE_DAYS = 5;
const TEN_DAYS = 10;
const EIGHTEEN_DAYS = 18;
const THIRTY_DAYS = 30;

export default function calc (days) {
    let cash = 0;
    for(let i = 1; i <= days; i++){
        if(i < FIVE_DAYS) cash += RATE
        else if (i < TEN_DAYS) cash += RATE/20*19
        else if (i < EIGHTEEN_DAYS) cash += RATE/10*9
        else if (i <= THIRTY_DAYS) cash += RATE/20*17 
    }
    return cash;
}