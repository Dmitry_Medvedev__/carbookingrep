import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Bookings } from './cars.entity';

@Entity()
export class Car {
    @PrimaryGeneratedColumn()
    id: number;   

    @Column({type: 'date'})
    startedDate: Date;

    @Column({type: 'date'})
    finishDate: Date;

    @ManyToOne(() => Bookings, bookings => bookings.cars)
    bookings: Bookings[];
}