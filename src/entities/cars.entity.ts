import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Car } from './car.entity';

@Entity()
export class Bookings {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    federalNum: string;

    @OneToMany(() => Car, car => car.bookings)
    cars: Car[];
}